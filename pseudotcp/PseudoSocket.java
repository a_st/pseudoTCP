package pseudotcp;

import pseudotcp.util.Sockets;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.URL;
import java.util.Scanner;
/**
 * ###################################
 * Server and socket are using udp.. Packet Loss is possible
 * ###################################
 * Socket to use for UDP-HolePunching
 * initialize through PseudoServer for server use or use with connect for the client!
 * @author Aaron Stein
 */
public class PseudoSocket {

    DatagramSocket i_socket;
    InetAddress address;
    int port; // port of opponent
    
    protected PseudoSocket(DatagramSocket i_socket, InetAddress address, int port) {
        this.i_socket = i_socket;
        this.address = address;
        this.port = port;
    }
    /**
     * connect client to server using my php script to exchange public ports
     * Usage ONLY allowed for small application (private) use or testing!
     * @param address server address
     * @param port your port (not important)
     * @return a new PseudoSocket to use in PseudoIn/Outputstreams
     * @throws IOException
     */
    public static PseudoSocket connect(InetAddress address, int port) throws IOException {
        DatagramSocket input_socket = Sockets.getInitSocket(address, port);
        int public_port = input_socket.getLocalPort();

        URL url = new URL("https://steppy-app.de/api/Pseudo.php?client=1&addr=" + address); //retrieve servers public port
        Scanner scanner = new Scanner(url.openStream());
        int server_port = scanner.nextInt();

        //send packet containing own localPort(Public port)
        byte[] pPort = String.valueOf(public_port).getBytes();

        DatagramSocket o_socket = new DatagramSocket();
        o_socket.send(new DatagramPacket(pPort, pPort.length, address, server_port));

        return new PseudoSocket(input_socket, address, server_port);
    }
    
     /**
     * connect client to server using given server_port
     * uses no other services!
     * @param address server address
     * @param port your port (not important)
     * @param server_port the servers public port
     * @return a new PseudoSocket to use in PseudoIn/Outputstreams
     * @throws IOException
     */
    public static PseudoSocket connect(InetAddress address, int port, int server_port) throws IOException {
        DatagramSocket input_socket = Sockets.getInitSocket(address, port);
        int public_port = input_socket.getLocalPort();

        //send packet containing own localPort(Public port)
        byte[] pPort = String.valueOf(public_port).getBytes();
        DatagramSocket o_socket = new DatagramSocket();
        o_socket.send(new DatagramPacket(pPort, pPort.length, address, server_port));

        return new PseudoSocket(input_socket, address, server_port);
    }
    
    public DatagramSocket getI_socket() {
        return i_socket;
    }

    public InetAddress getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

}
