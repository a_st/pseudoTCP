package pseudotcp.util;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to punch UDP-Hole and get DatagramSocket from it
 * @author Aaron Stein
 */
public class Sockets {
    /**
     * punches a udp-hole and return ready to use DatagramSocket
     * @param address the address you want to punch to (also doesn't really matter which one.. choose a trusted one)
     * @param port the internPort(sort of not important)
     * @return datagramSocket which is holePunched
     */
    public static DatagramSocket getInitSocket(InetAddress address, int port) {
        try {
            DatagramSocket i_socket = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(new byte[1], 1, address, port);
            i_socket.send(packet);
            return i_socket;
        } catch (IOException ex) {
            Logger.getLogger(Sockets.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
