package pseudotcp;

import pseudotcp.util.Sockets;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * class to create a Server which uses UDP-Punchole
 *  
 * @author Aaron Stein
 */
public class PseudoServer {

    private int port;

    private DatagramSocket input_socket;

    public PseudoServer(int port) {
        this.port = port;
    }

    /**
     * automated accept (shares public port automatically) usage limited for
     * testing
     *
     * @return PseudoSocket to use with Pseudo-IOStreams
     * @throws SocketException
     * @throws IOException
     */
    public PseudoSocket accept() throws SocketException, IOException {
        //send packet to punch the hole
        DatagramSocket i_socket = Sockets.getInitSocket(InetAddress.getByName("8.8.8.8"), port);

        int public_port = i_socket.getLocalPort();

        URL url = new URL("https://steppy-app.de/api/Pseudo.php?client=0&port=" + public_port);
        url.openStream(); //don't care about input because no output!

        //hole punched and public port of Server has been retrieved
        //client will also punch and sends confirmation packet
        DatagramPacket confPacket = new DatagramPacket(new byte[5], 5);
        i_socket.receive(confPacket);

        InetAddress client_address = confPacket.getAddress();
        int client_public_port = Integer.parseInt(new String(confPacket.getData()));
        return new PseudoSocket(i_socket, client_address, client_public_port);
    }

    /**
     * accept_ needs the client to know public port of server
     *
     * @return PseudoSocket to use with Pseudo-IOStreams
     * @throws SocketException
     * @throws IOException
     */
    public PseudoSocket accept_() throws SocketException, IOException {
        //send packet to punch the hole
        DatagramSocket i_socket = input_socket;

        //client will also punch and sends confirmation packet
        DatagramPacket confPacket = new DatagramPacket(new byte[5], 5);
        i_socket.receive(confPacket);

        InetAddress client_address = confPacket.getAddress();
        int client_public_port = Integer.parseInt(new String(confPacket.getData()));
        return new PseudoSocket(i_socket, client_address, client_public_port);
    }

    /**
     * use when using not only for testing and also share the port you receive
     * and give it the client
     *
     * @return public port use with accept_ method
     */
    public int open() {
        try {
            DatagramSocket i_socket = Sockets.getInitSocket(InetAddress.getByName("8.8.8.8"), port);
            this.input_socket = i_socket;
            return i_socket.getLocalPort();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PseudoServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}
