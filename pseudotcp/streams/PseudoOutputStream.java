package pseudotcp.streams;

import pseudotcp.PseudoSocket;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;

/**
 * Use this OutputStream like normal outputstream EXCEPT: call flush(); every time after sending!
 * @author Aaron Stein
 */
public class PseudoOutputStream extends OutputStream {

    PseudoSocket socket;
    byte[] buffer;
    int index = 4;
    int bufferSize;
    
    /**
     * create PseudoOutputStream from Socket
     * @param socket the prepared Socket
     * @param bufferSize each packets size + 5 header bytes
     */
    public PseudoOutputStream(PseudoSocket socket, int bufferSize) {
        this.socket = socket;
        this.bufferSize = bufferSize;
        buffer = new byte[bufferSize + 5];
    }

    @Override
    public void flush() throws IOException {
        send();
        super.flush();
    }

    @Override
    public void write(int b) throws IOException {
        if (index == bufferSize) {
            buffer[bufferSize] = 1; //if the end is reached add a 1 at the end as a signal that the next buffer is also related to this one!
            send();
        }
        buffer[index] = (byte) b;
        index++;
    }
    
    int counter = 0;
    
    private void send() throws SocketException, IOException {
        //number
        byte[] bytes = ByteBuffer.allocate(4).putInt(counter).array();
        counter++;
        System.arraycopy(bytes, 0, buffer, 0, 4);
        DatagramSocket dataSocket = new DatagramSocket();
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, socket.getAddress(), socket.getPort());
        dataSocket.send(packet);
        //reset buffer and index
        buffer = new byte[bufferSize + 5];
        index = 4;
    }

}
