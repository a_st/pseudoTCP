package pseudotcp.streams;

import pseudotcp.PseudoSocket;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Aaron Stein
 */
public class PseudoInputStream extends InputStream {

    PseudoSocket socket;
    volatile byte[] buffer;
    volatile List<byte[]> buffers = new ArrayList<>();
    int index = 4; // first 4 bytes is integer which counts packets
    int bufferIndex = 0;
    int bufferSize;
    Thread thread;

    volatile boolean wait = true;

    final Logger logger;
    
    /**
     * create PseudoInputStream from Socket
     * @param socket the prepared Socket
     * @param bufferSize each packets size + 5 header bytes
     * @param logger the logger you want to use
     */
    public PseudoInputStream(PseudoSocket socket, int bufferSize, Logger logger) {
        this.logger = logger;
        this.socket = socket;
        this.bufferSize = bufferSize;
        buffer = new byte[bufferSize + 5];

        thread = new Thread(() -> {
            try {
                DatagramSocket i_socket = socket.getI_socket();
                while (!thread.isInterrupted()) {
                    byte[] temp_buffer = new byte[this.bufferSize + 5];
                    DatagramPacket packet = new DatagramPacket(temp_buffer, temp_buffer.length);
                    i_socket.receive(packet);
                    buffers.add(temp_buffer);
                    int count = ByteBuffer.allocate(4).wrap(Arrays.copyOfRange(temp_buffer, 0, 4)).getInt();
                    System.out.println(count + ": " + Arrays.hashCode(temp_buffer));
                    index = 4;
                    wait = false;
                }
            } catch (IOException ex) {
                logger.severe(ex.getMessage());
            }
        });
        thread.setDaemon(true);
        thread.start();
    }
    
    /**
     * create PseudoInputStream from Socket
     * 
     * default Logger is used(class name)
     * @param socket the prepared Socket
     * @param bufferSize each packets size + 5 header bytes
     */
    public PseudoInputStream(PseudoSocket socket, int bufferSize) {
        this(socket, bufferSize, Logger.getLogger(PseudoInputStream.class.getName()));
    }

    @Override
    public int read() throws IOException {
        while (wait) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                logger.severe(ex.getMessage());
            }
        }
        boolean end = true;
        for (int i = index; i < buffer.length; i++) {
            if (buffer[i] != 0) {
                end = false;
                break;
            }
        }
        if (end) {
            logger.info("next buffer loaded beacuse of end..!");
            nextBuffer();
        } else if (index == bufferSize && buffer[bufferSize] == 1) {
            logger.info("next buffer loaded beacuse of end reached without 0..");
            nextBuffer();
        }

        return buffer[index++];

    }

    private void nextBuffer() {
        //take next from buffers
        index = 4;

        if (buffers.size() > 0) {
//            logger.info("next buffer loaded!");
            buffer = buffers.get(0);
            buffers.remove(0);
        } else {
            wait = true;
            while (wait) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    logger.severe(ex.getMessage());
                }
            }
            buffer = buffers.get(0);
            buffers.remove(0);
        }
    }

}
